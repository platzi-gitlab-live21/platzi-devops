Summary

(dar el resumen del issue)

Steps to reproduce

(indíca los pasos para reproducir el bug)

What is the current behavior?

(Comportamiento actual)

what is the expected behavior?

(Comportamiento esperado)